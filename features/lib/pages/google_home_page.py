from selenium.webdriver.common.by import By
from base_page_object import BasePage


class GoogleHomePage(BasePage):

    def __init__(self, context):
        BasePage.__init__(
            self,
            context.browser,
            base_url='http://www.google.co.uk')

    locator_dictionary = {
        "for_sale_tab": (By.LINK_TEXT, 'For sale'),
        "search_box": (By.ID, "search-input-location"),
        "search_button:": (By.ID, "search-submit")
    }

    def type_search(self, word):
        print(word)
        self.search_box.send_keys(word)
